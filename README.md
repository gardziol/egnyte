# README #

### What is this repository for? ###

* Quick summary

Egnyte is simple service to receiving simplified information about Places on facebook.

* Version

1.0.0

### How do I get set up? ###

* Summary of set up

Clone repository and build using Maven (see "Deployment instructions").

* Configuration

No configuration is needed.

* Dependencies

This project requires JDK in version 1.8, Apache Maven 3

* Database configuration

Does not require database.

* How to run tests

Run in root directory command "mvn test".

* Deployment instructions

To start service run in root directory command "mvn spring-boot:run"

### Who do I talk to? ###

* Repo owner

Jakub Gardo
