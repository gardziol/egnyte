package pl.gardo.jakub.egnyte.facebook.service.mapper;

import static org.junit.Assert.*;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.social.facebook.api.Location;

import pl.gardo.jakub.egnyte.facebook.service.model.Place;

public class PlaceMapperTest {

	private static final String NAME = "name";
	private static final String NAME_VALUE = "value";

	private static final String LOCATION = "location";
	
	private static final double LATITUDE = 12d;
	private static final double LONGITUDE = 12d;
	
	private FacebookPlaceMapper placeMapper;
	
	@Before
	public void setUp() throws Exception {
		placeMapper = new FacebookPlaceMapper();
	}

	@Test
	public void testValidPlace() {
//		given
		org.springframework.social.facebook.api.Place source = prepareSource(new Location(LATITUDE, LONGITUDE));
		
//		when
		Place target = placeMapper.map(source);
		
//		then
		assertEquals(NAME_VALUE, target.getName());
		assertTrue(LATITUDE == target.getLatitude());
		assertTrue(LONGITUDE == target.getLongitude());
	}

	private org.springframework.social.facebook.api.Place prepareSource(Location location) {
		org.springframework.social.facebook.api.Place source = new org.springframework.social.facebook.api.Place();
		setField(source, NAME, NAME_VALUE);
		setField(source, LOCATION, location);
		return source;
	}
	
	@Test
	public void testInvalidPlace() {
//		given
		org.springframework.social.facebook.api.Place source = prepareSource(null);

//		when
		Place target = placeMapper.map(source);
		
//		then
		assertEquals(NAME_VALUE, target.getName());
		assertTrue(0d == target.getLatitude());
		assertTrue(0d == target.getLongitude());
	}
	
	private void setField(Object object, String fieldName, Object value) {
		try {
			FieldUtils.writeDeclaredField(object, fieldName, value, true);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException(e);
		}
	}

}
