package pl.gardo.jakub.egnyte.facebook.validator;

import org.junit.Before;
import org.junit.Test;

public class CityNameValidatorTest {

	private Validator<String> validator;
	
	@Before
	public void setUp() throws Exception {
		validator = new CityNameValidator();
	}

	@Test
	public void testNotBlank() {
//		when
		validator.validate("NotBlank");
//		then
//		no exception
	}

	@Test(expected=IllegalArgumentException.class)
	public void testNull() {
//		when
		validator.validate(null);
//		then
//		exception expected
	}

	@Test(expected=IllegalArgumentException.class)
	public void testBlank() {
//		when
		validator.validate("  ");
//		then
//		exception expected
	}

}
