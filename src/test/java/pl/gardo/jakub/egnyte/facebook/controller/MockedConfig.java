package pl.gardo.jakub.egnyte.facebook.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import pl.gardo.jakub.egnyte.facebook.config.Constants;
import pl.gardo.jakub.egnyte.facebook.config.ConstantsImpl;

@Configuration
public class MockedConfig {

	@Bean
	@Primary
	public Constants constants() {
		return new ConstantsImpl("http://localhost:1080", "nevermind", "nevermind");
	}
}
