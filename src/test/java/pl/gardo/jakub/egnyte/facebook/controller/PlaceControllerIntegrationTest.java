package pl.gardo.jakub.egnyte.facebook.controller;

import static org.hamcrest.Matchers.is;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import pl.gardo.jakub.egnyte.EgnyteApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(
  webEnvironment = WebEnvironment.RANDOM_PORT,
  classes = EgnyteApplication.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = MockedConfig.class)
public class PlaceControllerIntegrationTest {

	private static final String POLAND_POZNAN_EGNYTE = "/poland/poznan/egnyte";

	@Autowired
	private MockMvc mvc;
 
	private static ClientAndServer mockServer;
	
	@BeforeClass
	public static void startProxy() {
	    mockServer = startClientAndServer(1080);
	    
	    mockServer.when(
	            request()
	                .withMethod("GET")
	                .withPath("/search")
		        ).respond(
		            response()
		                .withStatusCode(200)
		                .withHeader(Header.header("Content-Type", "application/json;charset=UTF-8"))
		                .withBody("{\"data\": [{\"location\": {\"city\": \"Poznan\",\"country\": \"Poland\",\"latitude\": 52.40474913,\"longitude\": 16.940680915,\"zip\": \"61-854\"},\"name\": \"Egnyte Poland\",\"id\":\"1458558184372813\"}]}")
		        );
	    mockServer.when(
	            request()
	                .withMethod("GET")
	                .withPath("/oauth/access_token")
		        ).respond(
		            response()
		                .withStatusCode(200)
		                .withHeader(Header.header("Content-Type", "application/json;charset=UTF-8"))
		                .withBody("{\"access_token\": \"someToken\", \"token_type\":\"bearer\"}")
		        );

	}
	
	@AfterClass
	public static void stopProxy() {
	    mockServer.stop();
	}
	
	@Test
	public void testValidRequest() throws Throwable {
		mvc.perform(get(POLAND_POZNAN_EGNYTE)
				  .contentType(MediaType.APPLICATION_JSON))
				  .andExpect(status().isOk())
				  .andExpect(content()
				  .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				  .andExpect(jsonPath("$[0].name", is("Egnyte Poland")))
				  .andExpect(jsonPath("$[0].latitude", is(52.40474913)))
				  .andExpect(jsonPath("$[0].longitude", is(16.940680915)))
				  ;
	}
	
}
