package pl.gardo.jakub.egnyte.facebook.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Test;
import org.springframework.social.facebook.api.Location;
import org.springframework.social.facebook.api.Place;

import pl.gardo.jakub.egnyte.facebook.repository.FacebookRepository;
import pl.gardo.jakub.egnyte.facebook.service.mapper.FacebookPlaceMapper;
import pl.gardo.jakub.egnyte.facebook.validator.CityNameValidator;
import pl.gardo.jakub.egnyte.facebook.validator.CountryNameValidator;
import pl.gardo.jakub.egnyte.facebook.validator.DescriptionValidator;

public class FacebookServiceImplTest {

	private static final String LOCATION = "location";
	private static final String CITY = "city";
	private static final String COUNTRY = "country";

	private static final String CITY_VALUE = "city";
	private static final String COUNTRY_VALUE = "country";

	private static final String OTHER_CITY_VALUE = "otherCity";
	private static final String OTHER_COUNTRY_VALUE = "otherCountry";

	private FacebookServiceImpl facebookService;

	@Test
	public void filterByCriteriaTest() {
//		given
		initializeFacebookService();

		Location location = createLocation(COUNTRY_VALUE, CITY_VALUE);
		Place place = createPlace(location);
//		when
		boolean result = facebookService.filterByCriterias(COUNTRY_VALUE, CITY_VALUE, place);
//		then
		assertTrue(result);
	}

	@Test
	public void filterWhenCriteriaAreNotInsuficcientTest() {
//		given
		initializeFacebookService();

		Location location = createLocation(COUNTRY_VALUE, CITY_VALUE);
		Place place = createPlace(location);
//		when
		boolean result = facebookService.filterByCriterias(OTHER_COUNTRY_VALUE, OTHER_CITY_VALUE, place);
//		then
		assertFalse(result);
	}

	@Test
	public void filterWhenLocationIsNullTest() {
//		given
		initializeFacebookService();

		Location location = null;
		Place place = createPlace(location);
//		when
		boolean result = facebookService.filterByCriterias(OTHER_COUNTRY_VALUE, OTHER_CITY_VALUE, place);
//		then
		assertFalse(result);
	}

	private Location createLocation(String countryValue, String cityValue) {
		Location location = new Location("nevermindDescription");
		setField(location, COUNTRY, countryValue);
		setField(location, CITY, cityValue);
		return location;
	}

	private Place createPlace(Location location) {
		Place place = new Place();
		
		setField(place, LOCATION, location);
		return place;
	}

	private void initializeFacebookService() {
		facebookService = new FacebookServiceImpl(mock(CountryNameValidator.class), 
				mock(CityNameValidator.class),
				mock(DescriptionValidator.class), 
				mock(FacebookRepository.class), 
				mock(FacebookPlaceMapper.class));
	}

	private void setField(Object object, String fieldName, Object value) {
		try {
			FieldUtils.writeDeclaredField(object, fieldName, value, true);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException(e);
		}
	}

}
