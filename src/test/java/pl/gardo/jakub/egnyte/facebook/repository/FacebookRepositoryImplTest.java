package pl.gardo.jakub.egnyte.facebook.repository;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import org.apache.commons.lang3.StringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;
import org.springframework.social.facebook.api.Location;
import org.springframework.social.facebook.api.Place;
import org.springframework.util.Assert;

import pl.gardo.jakub.egnyte.facebook.config.Constants;
import pl.gardo.jakub.egnyte.facebook.oauth.TokenProvider;
import pl.gardo.jakub.egnyte.facebook.repository.model.ResultWrapper;
import pl.gardo.jakub.egnyte.facebook.service.model.PlaceCriteria;

public class FacebookRepositoryImplTest {
	private static final String MOCKED_GRAPH_API_URL = "http://localhost:1080";
	private static final String VALID_TOKEN = "360360534424771|NNWu9Y9U3QsaEgacA33OGXHVQYI";
	
	private static ClientAndServer mockServer;
	
	@BeforeClass
	public static void startProxy() {
	    mockServer = startClientAndServer(1080);
	    
	    mockServer.when(
	            request()
	                .withMethod("GET")
	                .withPath("/search")
		        ).respond(
		            response()
		                .withStatusCode(200)
		                .withHeader(Header.header("Content-Type", "application/json;charset=UTF-8"))
		                .withBody("{\"data\": [{\"location\": {\"city\": \"Poznan\",\"country\": \"Poland\",\"latitude\": 52.40474913,\"longitude\": 16.940680915,\"zip\": \"61-854\"},\"name\": \"Egnyte Poland\",\"id\":\"1458558184372813\"}]}")
		        );
	}
	
	@AfterClass
	public static void stopProxy() {
	    mockServer.stop();
	}
	
	@Test
	public void testQueryWithProperValue() {
//		given
		TokenProvider tokenProvider = mock(TokenProvider.class);
		when(tokenProvider.provideToken()).thenReturn(VALID_TOKEN);
		
		Constants constants = mock(Constants.class);
		when(constants.getGraphApiUrl()).thenReturn(MOCKED_GRAPH_API_URL);
		
		FacebookFactory facebookFactory = new FacebookFactory();				
		FacebookRepository facebookRepository = new FacebookRepositoryImpl(constants, 
				tokenProvider, facebookFactory);

//		when
		PlaceCriteria criteria = new PlaceCriteria("poland", "poznan", "egnyte"); 
		
		ResultWrapper resultWrapper = facebookRepository.fetchPlaces(criteria);
				
//		then
		assertTrue(isValid(resultWrapper));
	}
	
	private boolean isValid(ResultWrapper resultWrapper) {
		Assert.notNull(resultWrapper, "resultWrapper is null");
		Assert.isTrue(!resultWrapper.getData().isEmpty(), "resultWrapper.data is empty");

		Place firstPlace = resultWrapper.getData().iterator().next();
		Assert.isTrue(StringUtils.isNotBlank(firstPlace.getId()), "resultWrapper.data[0].id is blank");
		Assert.isTrue(StringUtils.isNotBlank(firstPlace.getName()), "resultWrapper.data[0].name is blank");

		Location location = firstPlace.getLocation();
		Assert.notNull(location.getCity(), "City is null");
		Assert.notNull(location.getCountry(), "Country is null");
		Assert.notNull(location.getLatitude(), "Latitude is null");
		Assert.notNull(location.getLongitude(), "Longitude is null");
		
		return true;
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNullQuery() {
//		given
		TokenProvider tokenProvider = mock(TokenProvider.class);
		when(tokenProvider.provideToken()).thenReturn(VALID_TOKEN);

		Constants constants = mock(Constants.class);
		when(constants.getGraphApiUrl()).thenReturn(MOCKED_GRAPH_API_URL);

		FacebookFactory facebookFactory = new FacebookFactory();				
		FacebookRepository facebookRepository = new FacebookRepositoryImpl(constants, 
				tokenProvider, facebookFactory);

//		when
		facebookRepository.fetchPlaces(null);
				
//		then
//		exception expected
	}
	

}
