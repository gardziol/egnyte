package pl.gardo.jakub.egnyte.facebook.oauth;

import static org.junit.Assert.assertNotNull;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;
import org.springframework.web.client.RestTemplate;

import pl.gardo.jakub.egnyte.facebook.config.Constants;
import pl.gardo.jakub.egnyte.facebook.config.ConstantsImpl;
import pl.gardo.jakub.egnyte.facebook.oauth.exception.InvalidOAuthCredentials;

public class OAuthTokenProviderTest {
	private static final String MOCKED_GRAPH_API_URL = "http://localhost:1080";
	private static final String VALID_APP_ID = "360360534424771";
	private static final String VALID_APP_SECRET = "c5669213d24185cdb7d6be9a9d39789d";

	private static final String INVALID_APP_SECRET = "INVALID_APP_SECRET";

	private static ClientAndServer mockServer;
	
	@BeforeClass
	public static void startProxy() {
	    mockServer = startClientAndServer(1080);
	    
	    mockServer.when(
	            request()
	                .withMethod("GET")
	                .withPath("/oauth/access_token")
	                .withQueryStringParameter("client_secret", VALID_APP_SECRET)
		        ).respond(
		            response()
		                .withStatusCode(200)
		                .withHeader(Header.header("Content-Type", "application/json;charset=UTF-8"))
		                .withBody("{\"access_token\": \"someToken\", \"token_type\":\"bearer\"}")
		        );

	    mockServer.when(
	            request()
	                .withMethod("GET")
	                .withPath("/oauth/access_token")
	                .withQueryStringParameter("client_secret", INVALID_APP_SECRET)
		        ).respond(
		            response()
		                .withStatusCode(400)
		                .withHeader(Header.header("Content-Type", "application/json;charset=UTF-8"))
		                .withBody("{\"error\":{\"message\": \"Error validating client secret.\",\"type\": \"OAuthException\",\"code\": 1,\"fbtrace_id\": \"G+4x3/Fmncp\"}}")
		        );
	}
	
	@AfterClass
	public static void stopProxy() {
	    mockServer.stop();
	}

	@Test
	public void testProvidingToken() {
		// given
		Constants constants = new ConstantsImpl(MOCKED_GRAPH_API_URL, VALID_APP_ID, VALID_APP_SECRET);
		
		OAuthTokenProvider provider = new OAuthTokenProvider(constants, 
				new RestTemplate());
		// when
		String token = provider.provideToken();
		
		// then
		assertNotNull(token);
	}
	
	@Test(expected=InvalidOAuthCredentials.class)
	public void testWrongCredentials() {
		// given
		Constants constants = new ConstantsImpl(MOCKED_GRAPH_API_URL, VALID_APP_ID, INVALID_APP_SECRET);
		
		OAuthTokenProvider provider = new OAuthTokenProvider(constants, 
				new RestTemplate());
		// when
		provider.provideToken();
		
		// then
		// exception expected
	}
}
