package pl.gardo.jakub.egnyte.facebook.validator;

public interface Validator<T> {
	void validate(T t);
}
