package pl.gardo.jakub.egnyte.facebook.repository;

import pl.gardo.jakub.egnyte.facebook.repository.model.ResultWrapper;
import pl.gardo.jakub.egnyte.facebook.service.model.PlaceCriteria;

public interface FacebookRepository {

	ResultWrapper fetchPlaces(PlaceCriteria criteria);

}