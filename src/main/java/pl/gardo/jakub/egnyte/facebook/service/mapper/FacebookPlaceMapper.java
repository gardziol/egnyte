package pl.gardo.jakub.egnyte.facebook.service.mapper;

import org.springframework.social.facebook.api.Location;
import org.springframework.stereotype.Component;

import pl.gardo.jakub.egnyte.facebook.service.model.Place;

@Component
public class FacebookPlaceMapper implements Mapper<org.springframework.social.facebook.api.Place, Place> {
	public Place map(org.springframework.social.facebook.api.Place facebookPlace) {
		Location location = facebookPlace.getLocation();
		return new Place(facebookPlace.getName(), 
				location != null ? location.getLatitude() : 0d, 
				location != null ? location.getLongitude() : 0d);
	}	
}
