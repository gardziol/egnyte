package pl.gardo.jakub.egnyte.facebook.repository;

import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriTemplateHandler;

@Service
class FacebookFactory {

	public Facebook createFacebook(String accessToken) {
		FacebookTemplate facebook = new FacebookTemplate(accessToken);
		RestTemplate restTemplate = facebook.getRestTemplate();
		DefaultUriTemplateHandler handler = new DefaultUriTemplateHandler();
		handler.setStrictEncoding(true);
		restTemplate.setUriTemplateHandler(handler);

		return facebook;
	}
	
}
