package pl.gardo.jakub.egnyte.facebook.repository;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import org.springframework.web.util.UriComponentsBuilder;

import pl.gardo.jakub.egnyte.facebook.config.Constants;
import pl.gardo.jakub.egnyte.facebook.oauth.TokenProvider;
import pl.gardo.jakub.egnyte.facebook.repository.model.ResultWrapper;
import pl.gardo.jakub.egnyte.facebook.service.model.PlaceCriteria;

@Repository
public class FacebookRepositoryImpl implements FacebookRepository {

	private static final String QUERY_PARAM_NAME = "q";
	private static final String TYPE_PARAM_NAME = "type";
	private static final String FIELDS_PARAM_NAME = "fields";
	private static final String LIMIT_PARAM_NAME = "limit";

	private static final String QUERY_FORMAT = "%s%%26%s%%26%s";
	private static final String PLACE_TYPE = "place";
	private static final String PLACE_FIELDS = "location,name";
	private static final String LIMIT = "200";
	
	private final Constants constants;
	private final TokenProvider tokenProvider;
	
	private final FacebookFactory facebookFactory;	

	@Autowired
	public FacebookRepositoryImpl(Constants constants, 
			TokenProvider tokenProvider, FacebookFactory facebookFactory) {
		Assert.notNull(constants, "constants is null");
		Assert.notNull(tokenProvider, "tokenProvider is null");
		Assert.notNull(facebookFactory, "facebookFactory is null");

		this.constants = constants;
		this.tokenProvider = tokenProvider;
		this.facebookFactory = facebookFactory;
	}
	
	@Override
	public ResultWrapper fetchPlaces(PlaceCriteria criteria) {
		validateCriteria(criteria);
		
		String accessToken = tokenProvider.provideToken();		
		Facebook facebook = facebookFactory.createFacebook(accessToken);
		
		String url = prepareUrl(criteria);

		ResultWrapper resultWrapper = facebook.restOperations().getForObject(url, ResultWrapper.class);
		
		return resultWrapper;
	}

	private void validateCriteria(PlaceCriteria criteria) {
		Assert.notNull(criteria, "criteria is null");
		Assert.isTrue(StringUtils.isNotBlank(criteria.getCountry()), "Country is empty");
		Assert.isTrue(StringUtils.isNotBlank(criteria.getCity()), "City is empty");
		Assert.isTrue(StringUtils.isNotBlank(criteria.getDescription()), "Description is empty");
	}

	private String prepareUrl(PlaceCriteria criteria) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(constants.getGraphApiUrl() + "/search")
				.queryParam(QUERY_PARAM_NAME, String.format(QUERY_FORMAT, criteria.getCountry(), criteria.getCity(), criteria.getDescription()))
				.queryParam(TYPE_PARAM_NAME, PLACE_TYPE)
				.queryParam(FIELDS_PARAM_NAME, PLACE_FIELDS)
				.queryParam(LIMIT_PARAM_NAME, LIMIT)				
				;
		return builder.build(true).toUriString();
	}
	
	
}
