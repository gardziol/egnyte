package pl.gardo.jakub.egnyte.facebook.validator;

import org.springframework.stereotype.Component;

@Component
public class CityNameValidator implements Validator<String>{

	private final NotBlankValidator notBlankValidator;
	public CityNameValidator() {
		this.notBlankValidator = new NotBlankValidator("City name is blank");
	}

	@Override
	public void validate(String cityName) {
		notBlankValidator.validate(cityName);
	}
}
