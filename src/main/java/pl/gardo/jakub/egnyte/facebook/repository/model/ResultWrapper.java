package pl.gardo.jakub.egnyte.facebook.repository.model;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import org.springframework.social.facebook.api.Place;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ResultWrapper {
	private final Collection<Place> data;
	private final Paging paging;

	@JsonCreator
	public ResultWrapper(@JsonProperty(value = "data") Collection<Place> data,
			@JsonProperty(value="paging", required=false) Paging paging) {
		this.data = Collections.unmodifiableCollection(data);
		this.paging = paging;
	}

	public Collection<Place> getData() {
		return data;
	}
	
	public Optional<Paging> getPaging() {
		return Optional.ofNullable(paging);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((paging == null) ? 0 : paging.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultWrapper other = (ResultWrapper) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (paging == null) {
			if (other.paging != null)
				return false;
		} else if (!paging.equals(other.paging))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ResultWrapper [data=" + data + ", paging=" + paging + "]";
	}
}
