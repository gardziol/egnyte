package pl.gardo.jakub.egnyte.facebook.controller.mapper;

public interface Serializer<FROM, TO> {
	TO map(FROM source);
}