package pl.gardo.jakub.egnyte.facebook.oauth;

public interface TokenProvider {
	String provideToken();
}
