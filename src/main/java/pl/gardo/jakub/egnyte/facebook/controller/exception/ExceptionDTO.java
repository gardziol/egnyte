package pl.gardo.jakub.egnyte.facebook.controller.exception;


public class ExceptionDTO {
	private ExceptionCode exceptionCode;
	private String description;
	
	public ExceptionDTO(ExceptionCode exceptionCode, String description) {
		this.exceptionCode = exceptionCode;
		this.description = description;
	}

	public ExceptionCode getExceptionCode() {
		return exceptionCode;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((exceptionCode == null) ? 0 : exceptionCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExceptionDTO other = (ExceptionDTO) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (exceptionCode != other.exceptionCode)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExceptionDTO [exceptionCode=" + exceptionCode + ", description=" + description + "]";
	}
}