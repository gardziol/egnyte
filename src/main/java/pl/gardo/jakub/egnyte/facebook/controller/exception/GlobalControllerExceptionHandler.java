package pl.gardo.jakub.egnyte.facebook.controller.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {
	
	private static final Log LOG = LogFactory.getLog(GlobalControllerExceptionHandler.class);

	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
		String description = ex.getMessage();
	
		ExceptionDTO exceptionDto = new ExceptionDTO(ExceptionCode.ILLEGAL_ARGUMENT_EXCEPTION, description);
		return handleInternal(ex, request, exceptionDto, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<Object> handleHttpClientException(RuntimeException ex, WebRequest request) {
		LOG.error("Internal error.", ex);
		
		String description = "Internal error";
	
		ExceptionDTO exceptionDto = new ExceptionDTO(ExceptionCode.INTERNAL_ERROR, description);
		return handleInternal(ex, request, exceptionDto, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	private ResponseEntity<Object> handleInternal(Exception ex, WebRequest request,
			ExceptionDTO exceptionDto, HttpStatus httpStatus) {
		return handleExceptionInternal(ex, exceptionDto, new HttpHeaders(), httpStatus, request);
	}

}