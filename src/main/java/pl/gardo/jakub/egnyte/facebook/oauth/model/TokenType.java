package pl.gardo.jakub.egnyte.facebook.oauth.model;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum TokenType {
	BEARER("bearer")
	;
	
	private final String enumValue;

	private TokenType(String enumValue) {
		Assert.isTrue(StringUtils.isNotBlank(enumValue), "enumValue is null");
		this.enumValue = enumValue;
	}

	@JsonCreator
	public static TokenType forEnumValue(String source) {
		for (TokenType type : values()) {
			if (type.getEnumValue().equals(source));
		}
		throw new IllegalArgumentException("Unknown TokenType: " + source);
	}
	
	public String getEnumValue() {
		return enumValue;
	}
}
