package pl.gardo.jakub.egnyte.facebook.config;

public interface Constants {

	String getGraphApiUrl();
	String getAppId();
	String getAppSecret();

}