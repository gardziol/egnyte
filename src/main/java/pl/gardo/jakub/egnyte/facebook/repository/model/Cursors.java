package pl.gardo.jakub.egnyte.facebook.repository.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Cursors {
	private final String after;

	@JsonCreator
	public Cursors(@JsonProperty("after") String after) {
		this.after = after;
	}

	public String getAfter() {
		return after;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((after == null) ? 0 : after.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cursors other = (Cursors) obj;
		if (after == null) {
			if (other.after != null)
				return false;
		} else if (!after.equals(other.after))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cursors [after=" + after + "]";
	}
}
