package pl.gardo.jakub.egnyte.facebook.oauth.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AccessToken {
	private final String accessToken;
	private final TokenType tokenType;
	@JsonCreator
	public AccessToken(@JsonProperty("access_token") String accessToken, @JsonProperty("token_type") TokenType tokenType) {
		super();
		this.accessToken = accessToken;
		this.tokenType = tokenType;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public TokenType getTokenType() {
		return tokenType;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accessToken == null) ? 0 : accessToken.hashCode());
		result = prime * result + ((tokenType == null) ? 0 : tokenType.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccessToken other = (AccessToken) obj;
		if (accessToken == null) {
			if (other.accessToken != null)
				return false;
		} else if (!accessToken.equals(other.accessToken))
			return false;
		if (tokenType != other.tokenType)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "AccessToken [accessToken=" + accessToken + ", tokenType=" + tokenType + "]";
	}
}
