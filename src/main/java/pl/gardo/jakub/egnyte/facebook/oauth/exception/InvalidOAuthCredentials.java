package pl.gardo.jakub.egnyte.facebook.oauth.exception;

public class InvalidOAuthCredentials extends IllegalArgumentException {	
	private static final long serialVersionUID = 411915630017854523L;
	private final String appClientId;

	public InvalidOAuthCredentials(String appClientId) {
		super("Invalid credentials for appClientId: " + appClientId);
		
		this.appClientId = appClientId;
	}

	public String getAppClientId() {
		return appClientId;
	}
}
