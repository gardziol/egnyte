package pl.gardo.jakub.egnyte.facebook.controller;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.gardo.jakub.egnyte.facebook.controller.dto.PlaceDTO;
import pl.gardo.jakub.egnyte.facebook.controller.mapper.PlaceDTOSerializer;
import pl.gardo.jakub.egnyte.facebook.service.FacebookService;
import pl.gardo.jakub.egnyte.facebook.service.model.Place;
import pl.gardo.jakub.egnyte.facebook.service.model.PlaceCriteria;

@RestController
@RequestMapping
public class PlaceController {
	
	private final PlaceDTOSerializer placeMapper;
	private final FacebookService facebookService;
	
	public PlaceController(PlaceDTOSerializer placeMapper, FacebookService facebookService) {
		super();
		this.placeMapper = placeMapper;
		this.facebookService = facebookService;
	}

	@GetMapping(path="/{country}/{city}/{description}")
	public Collection<PlaceDTO> getPlaces(@PathVariable String country, 
			@PathVariable String city, 
			@PathVariable String description){
		
		PlaceCriteria criteria = new PlaceCriteria(country, city, description); 
		Collection<Place> places = facebookService.getPlaces(criteria);
		
		Collection<PlaceDTO> placeDTOs = places.stream()
				.map(placeMapper::map)
				.collect(Collectors.toSet());
		
		return placeDTOs;
	}
}
