package pl.gardo.jakub.egnyte.facebook.oauth.model;

public enum GrantType {
	CLIENT_CREDENTIALS("client_credentials")
	;
	private final String value;

	private GrantType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
