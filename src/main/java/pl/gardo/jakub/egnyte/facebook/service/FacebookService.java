package pl.gardo.jakub.egnyte.facebook.service;

import java.util.Collection;

import pl.gardo.jakub.egnyte.facebook.service.model.Place;
import pl.gardo.jakub.egnyte.facebook.service.model.PlaceCriteria;

public interface FacebookService {

	Collection<Place> getPlaces(PlaceCriteria criteria);

}