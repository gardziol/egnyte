package pl.gardo.jakub.egnyte.facebook.validator;

import org.springframework.stereotype.Component;

@Component
public class CountryNameValidator implements Validator<String>{
	
	private final NotBlankValidator notBlankValidator;
	public CountryNameValidator() {
		this.notBlankValidator = new NotBlankValidator("Country name is blank");
	}
	
	@Override
	public void validate(String countryName) {
		notBlankValidator.validate(countryName);
	}
}
