package pl.gardo.jakub.egnyte.facebook.service;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import pl.gardo.jakub.egnyte.facebook.repository.FacebookRepository;
import pl.gardo.jakub.egnyte.facebook.repository.model.ResultWrapper;
import pl.gardo.jakub.egnyte.facebook.service.mapper.Mapper;
import pl.gardo.jakub.egnyte.facebook.service.model.Place;
import pl.gardo.jakub.egnyte.facebook.service.model.PlaceCriteria;
import pl.gardo.jakub.egnyte.facebook.validator.CityNameValidator;
import pl.gardo.jakub.egnyte.facebook.validator.CountryNameValidator;
import pl.gardo.jakub.egnyte.facebook.validator.DescriptionValidator;

@Service
public class FacebookServiceImpl implements FacebookService {

	private final CountryNameValidator countryNameValidator;	
	private final CityNameValidator cityNameValidator;
	private final DescriptionValidator descriptionValidator;
	private final FacebookRepository facebookRepository;
	private final Mapper<org.springframework.social.facebook.api.Place, Place> placeMapper;
	
	@Autowired
	public FacebookServiceImpl(CountryNameValidator countryNameValidator, CityNameValidator cityNameValidator,
			DescriptionValidator descriptionValidator, FacebookRepository facebookRepository, Mapper<org.springframework.social.facebook.api.Place, Place> placeMapper) {
		Assert.notNull(countryNameValidator, "countryNameValidator is null");
		Assert.notNull(cityNameValidator, "cityNameValidator is null");
		Assert.notNull(descriptionValidator, "descriptionValidator is null");
		Assert.notNull(facebookRepository, "facebookRepository is null");
		Assert.notNull(placeMapper, "placeMapper is null");
		
		this.countryNameValidator = countryNameValidator;
		this.cityNameValidator = cityNameValidator;
		this.descriptionValidator = descriptionValidator;
		this.facebookRepository = facebookRepository;
		this.placeMapper = placeMapper;
	}

	@Override
	public Collection<Place> getPlaces(PlaceCriteria criteria) {
		validate(criteria);	
		
		ResultWrapper resultWrapper = facebookRepository.fetchPlaces(criteria);
		Collection<Place> result = filterAndMapPlace(criteria.getCountry(), criteria.getCity(), resultWrapper);
		
		return result;
	}
	
	private void validate(PlaceCriteria criteria) {
		countryNameValidator.validate(criteria.getCountry());
		cityNameValidator.validate(criteria.getCity());
		descriptionValidator.validate(criteria.getDescription());
	}	

	private Collection<Place> filterAndMapPlace(String country, String city, ResultWrapper resultWrapper) {
		Collection<Place> result = resultWrapper.getData().stream()
				.filter(p -> filterByCriterias(country, city, p))
				.map(placeMapper::map)
				.collect(Collectors.toList());
		return result;
	}

	boolean filterByCriterias(String country, String city, org.springframework.social.facebook.api.Place p) {
		return p.getLocation() != null
				&& country.equalsIgnoreCase(p.getLocation().getCountry())
				&& city.equalsIgnoreCase(p.getLocation().getCity());
	}
}
