package pl.gardo.jakub.egnyte.facebook.repository.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Paging {
	private final Cursors cursors;
	private final String next;
	
	@JsonCreator
	public Paging(@JsonProperty("cursors")Cursors cursors, @JsonProperty("next")String next) {
		this.cursors = cursors;
		this.next = next;
	}

	public Cursors getCursors() {
		return cursors;
	}

	public String getNext() {
		return next;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cursors == null) ? 0 : cursors.hashCode());
		result = prime * result + ((next == null) ? 0 : next.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paging other = (Paging) obj;
		if (cursors == null) {
			if (other.cursors != null)
				return false;
		} else if (!cursors.equals(other.cursors))
			return false;
		if (next == null) {
			if (other.next != null)
				return false;
		} else if (!next.equals(other.next))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Paging [cursors=" + cursors + ", next=" + next + "]";
	}
}
