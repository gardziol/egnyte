package pl.gardo.jakub.egnyte.facebook.service.mapper;

public interface Mapper<FROM, TO> {
	TO map(FROM source);
}