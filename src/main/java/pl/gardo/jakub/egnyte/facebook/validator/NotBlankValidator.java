package pl.gardo.jakub.egnyte.facebook.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

class NotBlankValidator implements Validator<String>{

	private final String message;

	public NotBlankValidator(String message) {
		this.message = message;
	}

	@Override
	public void validate(String value) {
		Assert.isTrue(StringUtils.isNotBlank(value), message);
	}
}
