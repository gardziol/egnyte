package pl.gardo.jakub.egnyte.facebook.validator;

import org.springframework.stereotype.Component;

@Component
public class DescriptionValidator implements Validator<String>{

	private final NotBlankValidator notBlankValidator;
	public DescriptionValidator() {
		this.notBlankValidator = new NotBlankValidator("Description is blank");
	}

	@Override
	public void validate(String description) {
		notBlankValidator.validate(description);
	}
}
