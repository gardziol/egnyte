package pl.gardo.jakub.egnyte.facebook.controller.mapper;

import org.springframework.stereotype.Component;

import pl.gardo.jakub.egnyte.facebook.controller.dto.PlaceDTO;
import pl.gardo.jakub.egnyte.facebook.service.model.Place;

@Component
public class PlaceDTOSerializer implements Serializer<Place, PlaceDTO> {
	public PlaceDTO map(Place place) {
		return new PlaceDTO(place.getName(), 
				place.getLatitude(), 
				place.getLongitude());
	}	
}
