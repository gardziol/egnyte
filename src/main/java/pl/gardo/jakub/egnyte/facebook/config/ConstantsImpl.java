package pl.gardo.jakub.egnyte.facebook.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class ConstantsImpl implements Constants {

	private final String graphApiUrl;
	private final String appId;
	private final String appSecret;
	
	@Autowired
	public ConstantsImpl(@Value("${facebook.graphApi.baseUrl}") String graphApiUrl,
			@Value("${spring.social.facebook.appId}") String appId, 
			@Value("${spring.social.facebook.appSecret}") String appSecret) {
		Assert.isTrue(StringUtils.isNotBlank(graphApiUrl), "graphApiUrl is blank");
		Assert.isTrue(StringUtils.isNotBlank(appId), "appId is blank");
		Assert.isTrue(StringUtils.isNotBlank(appSecret), "appSecret is blank");

		this.graphApiUrl = graphApiUrl;
		this.appId = appId;
		this.appSecret = appSecret;
	}

	@Override
	public String getGraphApiUrl() {
		return graphApiUrl;
	}

	@Override
	public String getAppId() {
		return appId;
	}

	@Override
	public String getAppSecret() {
		return appSecret;
	}
}
