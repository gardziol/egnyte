package pl.gardo.jakub.egnyte.facebook.oauth;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import pl.gardo.jakub.egnyte.facebook.config.Constants;
import pl.gardo.jakub.egnyte.facebook.oauth.exception.InvalidOAuthCredentials;
import pl.gardo.jakub.egnyte.facebook.oauth.model.AccessToken;
import pl.gardo.jakub.egnyte.facebook.oauth.model.GrantType;

@Component
public class OAuthTokenProvider implements TokenProvider {
	private static final String CLIENT_ID = "client_id";
	private static final String CLIENT_SECRET = "client_secret";
	private static final String GRANT_TYPE = "grant_type";

	private static final String ACCESS_TOKEN_ENDPOINT = "/oauth/access_token";
	
	private final Constants constants;
	private final RestTemplate restTemplate;

	@Autowired
	public OAuthTokenProvider(Constants constants,
			RestTemplate restTemplate) {
		Assert.notNull(restTemplate, "restTemplate is null");
		
		this.constants = constants;
		this.restTemplate = restTemplate;
	}

	@Override
	public String provideToken() {
		String url = constants.getGraphApiUrl() + ACCESS_TOKEN_ENDPOINT;
		
		HttpEntity<?> entity = prepareHttpEntity();		
		String uri = prepareUri(url);

		try {
			return doProvideToken(entity, uri);
		} catch (HttpClientErrorException e) {
			throw new InvalidOAuthCredentials(constants.getAppId());
		}
	}

	private String doProvideToken(HttpEntity<?> entity, String uri) {
		ResponseEntity<AccessToken> response = restTemplate.exchange(uri, HttpMethod.GET, entity, AccessToken.class);
		AccessToken accessToken = response.getBody();
		
		return accessToken.getAccessToken();
	}

	private String prepareUri(String baseUrl) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl)
				.queryParam(CLIENT_ID, constants.getAppId())
				.queryParam(CLIENT_SECRET, constants.getAppSecret())
				.queryParam(GRANT_TYPE, GrantType.CLIENT_CREDENTIALS.getValue())
				;
		return builder.toUriString();
	}

	private HttpEntity<?> prepareHttpEntity() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		HttpEntity<?> entity = new HttpEntity<Void>(headers);
		return entity;
	}
}
