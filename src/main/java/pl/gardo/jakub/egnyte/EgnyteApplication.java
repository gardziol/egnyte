package pl.gardo.jakub.egnyte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EgnyteApplication {
	public static void main(String[] args) {
		SpringApplication.run(EgnyteApplication.class, args);
	}
}
